<?php
include_once 'config/DB.php';
$DB=new Database;
$DB->connect();
$a=$DB->GetAllData('thanhvien');
if(isset($_GET['controller']))
{
    $controller=$_GET['controller'];
}
else {
    $controller=NULL;
}

switch ($controller) {
    case 'thanhvien':
        require_once 'controllers/ThanhVienController.php';
        break;

    default:
        echo 'Không tồn tại controller xử lý';
        break;
}
