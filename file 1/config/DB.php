<?php
class Database
{
    public $hostname = 'localhost';
    public $username = 'root';
    public $password= '';
    public $dbname = 'db_k74';
    public $conn=NULL;
    //kết nối
    public function connect()
    {
        $this->conn = new mysqli($this->hostname, $this->username, $this->password, $this->dbname);
        if(!$this->conn)
        {
            echo 'Kết nối thất bại!';
        }
        else {
            mysqli_set_charset($this->conn,'utf8');
        }

    }

    //thực thi
    public function execute($sql)
    {
      return $this->conn->query($sql);
        
    
    }

    // lấy toàn bộ dữ liệu trong 1 bảng
    public function GetAllData($table)
    {
        $result=$this->execute("select * from $table");
        $data=array();
        if(mysqli_num_rows($result) > 0)
        {
            while ($row = mysqli_fetch_object($result))  
            {
            $data[]= $row;
            } 
        }
        return $data;
    }








}
